# Binaries

This repository contains built binaries for my various projects.

This is temporary, I plan to create a proper download site for my projects.

## Note to Gitlab Staff

If I shouldn't be doing this, please email me at flero@flero.dev and I will take this repository down along with all the files as soon as I recieve an email. This isn't made to do anything malicious, but rather host binaries for my different projects. Every binary hosted here are open source on my Gitlab profile.
